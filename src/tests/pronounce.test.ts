import { expect } from "chai";
import { pronounce, splitTokens } from '../pronounce';

describe('pronounce', () => {
  const cases = [
    {
      // Use the American voice, not the British voice which sometimes skips
      // pronouncing 'r' sounds.
      text: 'four',
      parts: [{ tag: 'word', text: 'four', ipa: 'fˈoːɹ' }],
    },
    {  // Punctuation
      text: '"Chicken and waffles!"',
      parts: [
        { tag: 'extra', text: '"' },
        { tag: 'word', text: 'Chicken', ipa: 'tʃˈɪkɪn' },
        { tag: 'extra', text: ' ' },
        { tag: 'word', text: 'and', ipa: 'ænd' },
        { tag: 'extra', text: ' ' },
        { tag: 'word', text: 'waffles', ipa: 'wˈɑːfəlz' },
        { tag: 'extra', text: '!"' },
      ],
    },
    {  // Only count "'" as word-like if it's in the middle of a word
      text: "the 'ain't' club",
      parts: [
        { tag: 'word', text: 'the', ipa: 'ðə' },
        { tag: 'extra', text: " '" },
        { tag: 'word', text: "ain't", ipa: 'ˈeɪnt' },
        { tag: 'extra', text: "' " },
        { tag: 'word', text: 'club', ipa: 'klˈʌb' },
      ],
    },
    {  // Handle curly single-quotes ("’")
      text: 'I’ll return',
      parts: [
        { tag: 'word', text: 'I’ll', ipa: 'aɪl' },
        { tag: 'extra', text: ' ' },
        { tag: 'word', text: 'return', ipa: 'ɹᵻtˈɜːn' },
      ],
    },
    {  // Heteronyms should be recognized
      text: "can you read what i've read",
      parts: [
        { tag: 'word', text: 'can', ipa: 'kæn' },
        { tag: 'extra', text: ' ' },
        { tag: 'word', text: 'you', ipa: 'juː' },
        { tag: 'extra', text: ' ' },
        { tag: 'word', text: 'read', ipa: 'ɹˈiːd' },
        { tag: 'extra', text: ' ' },
        { tag: 'word', text: 'what', ipa: 'wʌt' },
        { tag: 'extra', text: ' ' },
        { tag: 'word', text: "i've", ipa: 'aɪv' },
        { tag: 'extra', text: ' ' },
        { tag: 'word', text: 'read', ipa: 'ɹˈɛd' },
      ],
    },
  ];

  for (const { text, parts } of cases) {
    specify(text, async () => {
      expect(await pronounce(text)).to.deep.equal(parts);
    });
  }
});

describe('splitTokens', () => {
  const cases = [
    { text: 'ab', tokens: [[true, 'ab']] },
    { text: '..', tokens: [[false, '..']] },
    { text: 'ab..cd', tokens: [[true, 'ab'], [false, '..'], [true, 'cd']] },
    { text: '.x.', tokens: [[false, '.'], [true, 'x'], [false, '.']] },
  ];

  for (const { text, tokens } of cases) {
    specify(text, () => {
      expect([...splitTokens(text)]).to.deep.equal(tokens);
    });
  }
});
