import { expect } from 'chai';
import supertest from 'supertest';
import { ServerRule } from './rules';

describe('/', () => {
  const serverRule = new ServerRule();

  specify('basic', async () => {
    const response = await supertest(serverRule.server).get('/');
    expect(response.status).to.equal(200);
    expect(response.text).to.contain('Vyeshal');
  });
});

describe('/api/v1/translate', () => {
  const serverRule = new ServerRule();

  specify('basic', async () => {
    const response = await supertest(serverRule.server)
      .get('/api/v1/translate?q=hello');
    expect(response.status).to.equal(200);
    expect(response.body.text).to.equal('folaw');
  });
});
