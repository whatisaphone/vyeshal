import { expect } from "chai";
import { toVyeshal } from '../vyeshal';

describe('toVyeshal', () => {
  const cases = [
    /* Voice lines from the game */

    [  // un_act_squirrel_vo_cry_01
      'fire away',
      'hyol yevwoya',
    ],
    [  // un_act_squirrel_vo_cry_13
      'go go brothers',
      'daw daw ployeshalzh',
    ],
    [  // un_act_squirrel_vo_idl_01
      'need more orn',
      'noog vawl alyen',
    ],
    [  // un_act_squirrel_vo_idl_03
      'ninety nine bottles of orn on the wall',
      'nyontoo nyon pyawtyelzh yem alyen yawn vwoyawl',
      // The original inaccurately translated bottles as 'pyawtalzh'.
      // Bottles is /bɑɾəlz/, but they translated as if it were /bɑɾʊlz/.
    ],
    [  // un_act_squirrel_vo_idl_11
      'this is strong stuff',
      'shes ezh styelong styehye',
    ],
    [  // un_act_squirrel_vo_idl_12
      'next round on me',
      'nokst lowngye yawn voo',
    ],
    [  // un_act_squirrel_vo_idl_20
      'hello from the audio team',
      'folaw hyelyev yawgooaw toov',
      // The original 'folaw hyelyem yawgooaw toov' is inaccurate.
      // The m in 'hyelyem' should have become a v.
    ],
    [  // un_act_squirrel_vo_ord_01
      'liquid courage',
      'lekvweg kalez',
    ],
    [  // un_act_squirrel_vo_ord_11
      'yes sir',
      'zos sal',
    ],
    [  // un_act_squirrel_vo_spn_20
      'armed and dangerous',
      'yawlvgye ingye gyanzalyes',
    ],
    [  // un_act_squirrel_vo_vic_maj_02
      'right between the eyes',
      'loyot petvwoon yozh',
      // The original inaccurately translated between as 'pootvwoon'.
      // Between is /bᵻtwin/, but they translated as if it were /bitwin/
      // with the first syllable over-enunciated.
    ],
    [  // un_act_squirrel_vo_vic_out_03
      'did this just happen',
      'geg shes zoyest fibyen',
      // The original inaccurately translated 'happen' as 'fibon'.
      // Happen is /hæpən/, but they translated as if it were /hæpɛn/
      // with an over-enunciated 'e'.
    ],
    [  // un_act_lizard_vo_cry_07
      'fight',
      'hyot',
    ],
    [  // un_act_toad_vo_idl_03
      "i don't care anymore",
      'yo gawnt kol onoovawl',
      // The original 'yo gawnt kyal yenoovawl' is inaccurate.
      // 'care anymore' is /kɛɹ ɛnimoɹ/, but they translated as if it were
      // /keɪɹ ənimoɹ/.
    ],
    [  // un_act_toad_vo_idl_04
      'invisible touch',
      'enmezhepyel tyejye',
      // The original 'enmezhepal tajye' is inaccurate.
      // It's pronounced /ɪnvɪzᵻbəl tʌtʃ/, but they translated as if it were
      // /ɪnvɪzᵻbʊl tʊtʃ/.
    ],
    [  // un_act_toad_vo_idl_06
      'land of confusion',
      'lingye yem kyenhyezeejyen',
      // The original 'lingye yem kyawnhyezeezhen' is inaccurate.
      // Confusion is /kənfjuʒən/, but they translated as if it were /kɔnfjuzən/.
    ],
    [  // un_act_pigeon_vo_idl_01
      'rest now children',
      'lost now jelgloyen',
      // The original 'lost now jelglon' is inaccurate.
      // Children is /tʃɪldɹən/, but they translated as if it were /tʃɪldɹɛn/
      // with an over-enunciated 'e'. In 'jelglon', their 'o' should be a 'ye',
      // and then you need to add back another 'o' due to the vowel rule.
    ],
    [  // un_act_pigeon_vo_idl_05
      'everyone feeling okay',
      'omyelevwoyen hooleeng awkya',
    ],
    [  // un_act_pigeon_vo_idl_09
      'wonderful to be alive',
      'vwoyengalhal tee poo yeloyom',
    ],
    [  // un_act_mole_vo_cry_01
      'leeroy jenkins',
      'loolyoo zengkenzh',
      // The original 'loolyoo zongkenzh' is inaccurate.
      // The /ɛŋ/ in Jenkins should have become 'eng', but they translated it to
      // 'on' or possibly 'ong'. It's possible /ɛŋ/ is wrong in the chart since
      // that mistake has happened more than once? (See un_act_falcon_vo_cry_10)
    ],
    [  // un_act_mole_vo_cry_01
      "don't worry",
      'gawnt vwaloo',
    ],
    [  // un_act_mole_vo_cry_03
      'crime time',
      'kloyov tyov',
    ],
    [  // un_act_mole_vo_cry_13
      'smash',
      'svich',
    ],
    [  // un_act_mole_vo_ord_13
      'for freedom',
      'hawl hyeloogyev',
    ],
    [  // un_act_falcon_vo_cry_01
      'firing',
      'hyoleeng',
    ],
    [  // un_act_falcon_vo_cry_09
      'target acquired',
      'tyawldet yekvwoyolgye',
      // The original 'tyawldot' is inaccurate. Target is /tɑɹɡɪt/, but they
      // translated as if it were /tɑɹɡɛt/ with an over-enunciated 'e'.
    ],
    [  // un_act_falcon_vo_cry_10
      'engaging enemy',
      'engdyazeeng onyevoo',
      // The original 'ondyazeeng onovoo' is inaccurate.
      // Enemy is /ɛnəmi/, but they translated as if it were /ɛnɛmi/ with an
      // over-enunciated 'e'.
      //
      // My result is inaccurate for the moment as well. It treats engaging
      // /ɛnɡeɪdʒɪŋ/ if it were /ɛŋɡeɪdʒɪŋ/. The real answer is either
      // 'ondyazeeng' or 'endyazeeng' depending on your accent. I should fix.
    ],
    [  // un_act_falcon_vo_idl_01
      'ice water',
      'yos vwoyawtal',
    ],
    [  // un_act_falcon_vo_idl_02
      'i make this look good',
      'yo vyak shes lak dag',
    ],
    [  // un_act_falcon_vo_idl_03
      "you don't have time to think up here",
      'zee gawnt fim tyov tee sheengk yeb fool',
    ],
    [  // un_act_falcon_vo_sal_07
      'looking good chief',
      'lakeeng dag joohye',
    ],
    [  // un_act_falcon_vo_vic_maj_01
      'tango down, boys!',
      'tangaw gown, pyoozh!',
    ],
    [  // un_act_falcon_vo_vic_maj_03
      'got one eyes closed',
      'dyawt vwoyen yozh klawzhgye',
    ],
    [  // un_act_falcon_vo_vic_min_13
      'fish in a barrel',
      'hech en palyel',
      // The original 'hech en pyalal' (or 'pyalawl') is inaccurate.
      // Barrel is /bæɹəl/, but they translated as if it were either /bæɹoʊl/ or
      // /bæɹʊl/.
    ],
    [  // un_act_skunk_vo_idl_01
      "I think I've sprung a leak.",
      'Yo sheengk Yom sbloong look.',
      // The original is inaccurate. They translated 'I've' to 'Yov', forgetting
      // to change the 'v' to an 'm'.
    ],
    [  // un_act_skunk_vo_idl_06
      'Smell my finger',
      'Svol vyo heengal',
    ],
    [  // un_act_chameleon_vo_vic_out_09
      "don't mess with trench gang",
      'gawnt vos vwesh tyelonjye dang',
    ],
    [  // un_act_snake_vo_cry_08
      'payment is due',
      'byavyent ezh gee',
      // The original 'byavont ezh gee' is inaccurate.
      // Payment is /peɪmənt/, but they translated as if it were /peɪmɛnt/ with
      // an over-enunciated 'e'.
    ],
    [  // un_act_snake_vo_idl_07
      'do not mind the fine print',
      'gee nyawt vyongye hyon blent',
    ],
    [  // un_act_snake_vo_vic_maj_02
      'debt has been paid',
      'got fizh pen byag',
      // The original incorrectly translates been to 'poon'. Been is /bɪn/, but
      // they translated as if it were /bin/.
    ],
    [  // un_act_snake_vo_vic_out_06
      'payment received',
      'byavyent lesoomgye',
      // The original is 'byavont losoomgye', with both 'e's over-enunciated.
    ],

    /* Capitalization */

    ['Go', 'Daw'],
    ['go, go!', 'daw, daw!'],
    ['go go GO', 'daw daw DAW'],
    // For CamelCase words, we don't have enough metadata to capitalize
    // accurately, but Firstcap is better than nothing.
    ['GreatScott', 'Dlyatskyawt'],

    /* Capitalization fallback */

    // espeak will pronounce this as three words – ['go', 'asterisk', 'go'].
    // Our word splitter will see it as two words – ['go', 'go'], with some
    // trivia in between. Since the two word counts don't match, we can't match
    // up the IPA with the original text, and we can't preserve capitalization.
    // We fall back to transliterating it all at once, and the result will be
    // Firstcap, since the original text's first letter is capitalized.
    ['Go * Go', 'Daw istalesk daw'],

    /* Omit articles */

    ['a win is a win', 'vwen ezh vwen'],
    ['A win is a win', 'vwen ezh vwen'],
    ['give the dog the bone', 'dem gyawd pawn'],
    ['FOR THE WIN', 'HAWL VWEN'],

    /* Special-case to avoid 'shit' */

    ['that', 'shes'],
    ['THAT', 'SHES'],
    ['those', 'shoozh'],

    /* Workaround for espeak combining words */

    ['we shall', 'vwoo chil'],
    ['We Shall', 'Vwoo Chil'],
    ['I shall', 'Yo chil'],
    ['I am', 'Yo iv'],
    ['no one', 'naw vwoyen'],
    ['may have', 'vya fim'],
    ['no more', 'naw vawl'],
    ['there are', 'shol yawl'],

    /* Numbers should be preserved as numbers */

    ['from 0 to 60', 'hyelyev 0 tee 60'],

    /* espeak translates ',' to '\n', which we must handle */

    ['comma, comma', 'kyawvye, kyawvye'],

    /* Various observed mistakes */

    ['motherland', 'vyeshallingye'],
    ['desperation clear', 'gosbalyachyen klool'],
    ['Eenglech', 'Eenglojye'],
  ];

  for (const [english, vyeshal] of cases) {
    specify(english, async () => {
      const translated = await toVyeshal(english);
      expect(translated.vyeshal).to.equal(vyeshal);
    });
  }
});
