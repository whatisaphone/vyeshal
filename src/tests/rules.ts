import { Server } from 'http';
import { createServer } from '../server';

export class ServerRule {
  public server!: Server;

  constructor() {
    beforeEach(this.beforeEach);
    afterEach(this.afterEach);
  }

  private readonly beforeEach = () => {
    this.server = createServer();
  };

  private readonly afterEach = () => {
    this.server.close();
    delete this.server;
  };
}
