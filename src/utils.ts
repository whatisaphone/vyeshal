// istanbul ignore next
/**
 * Asserts at compile-time that a code path is unreachable.
 */
export function unreachable(_: never): never {
  throw new Error('bug');
}
