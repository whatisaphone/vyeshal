// istanbul ignore file

import Raven from 'raven';
import config from '../config';
import { createServer } from "./server";

function main() {
  Raven.config(config.sentryDSN).install();
  createServer();
}

if (module === require.main)
  main();
