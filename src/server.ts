import express, { NextFunction, Request, RequestHandler, Response } from 'express';
import Raven from 'raven';
import { toVyeshal } from './vyeshal';

export function createServer() {
  return express()
    .use(Raven.requestHandler())
    .get('/', home)
    .get('/api/v1/translate', translate)
    .use(Raven.errorHandler())
    .use(internalServerError)
    .listen(3000);
}

function asyncView(handler: (req: Request, res: Response) => Promise<void>): RequestHandler {
  return (req, res, next) => {
    handler(req, res).catch(next);
  };
}

function home(req: Request, res: Response) {
  res.send(`<!DOCTYPE html>
<title>Vyeshal translator</title>
<h1>Vyeshal translator</h1>
<form>
  <div><input id="english" size="50"></div>
  <div><button>Translate</button> <span id="status"></span></div>
</form>
<div></div>
<div>&nbsp;</div>
<div><input id="vyeshal" size="50" readonly></div>
<script>
  const form = document.querySelector('form');
  const input = document.getElementById('english');
  const status = document.getElementById('status');
  const result = document.getElementById('vyeshal');
  form.addEventListener('submit', handleSubmit);

  async function handleSubmit(e) {
    e.preventDefault();

    status.textContent = 'Loading';

    try {
      const url = '/api/v1/translate?q=' + encodeURIComponent(input.value);
      const response = await fetch(url);
      if (!response.ok)
        throw new Error();
      const body = await response.json();

      status.textContent = 'Done';
      result.value = body.text;
    } catch {
      status.textContent = 'Error';
      result.value = '';
    }
  }
</script>`);
}

const translate = asyncView(async (req, res) => {
  const translated = await toVyeshal(req.query.q);
  res.json({ text: translated.vyeshal, debug: translated.debug });
});

// istanbul ignore next
async function internalServerError(err: Error, req: Request, res: Response, next: NextFunction) {
  if (err) {
    res.status(500).contentType('text/plain;charset=utf-8');

    if (process.env.NODE_ENV !== 'production')
      res.send(err.stack);
    else
      res.send('500 ' + (await toVyeshal('Internal Server Error')).vyeshal);
  }
}
