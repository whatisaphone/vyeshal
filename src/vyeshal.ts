import { captureMessage } from 'raven';
import { Part, pronounce } from './pronounce';
import { unreachable } from './utils';

export async function toVyeshal(english: string) {
  const parts = await pronounce(censorShit(omitArticles(english)));
  const raw = [...parts.map(translate)].join('');
  const vyeshal = addO(addYe(raw)).replace(/\s{2,}/g, ' ');
  return { vyeshal, debug: parts };
}

/**
 * Notes from https://www.reddit.com/r/ToothAndTail/comments/76mvba/so_you_want_to_learn_vyeshal/
 *
 * any time you would use that or those, use this or these instead. The reason
 * is "that" would translate to "shit" ;)
 */
function censorShit(english: string) {
  return english
  .replace(/\bthat\b/ig, (m) => matchCase('this', m))
  .replace(/\bthose\b/ig, (m) => matchCase('these', m));
}

function translate(part: Part) {
  if (part.tag === 'word') {
    const vyeshal = [...transliterate(part.ipa)].join('');
    return matchCase(vyeshal, part.text);
  } else if (part.tag === 'extra') {
    return part.text;
  } else {
    // istanbul ignore next
    throw unreachable(part);
  }
}

function* transliterate(ipa: string) {
  for (const phoneme of tokenizeIPA(ipa))
    yield transliterations[phoneme];
}

const transliterations: { [ipa: string]: string } = {
  // Vowels
  'æ': 'i',
  'ɛ': 'o',
  'ɪ': 'e',
  'ᵻ': 'e',
  'o': 'aw',
  'oʊ': 'aw',
  'i': 'oo',
  'ɪɹ': 'ool',
  'u': 'ee',
  'ʊ': 'a',
  'aʊ': 'ow',
  'eɪ': 'ya',
  'ɑ': 'yaw',
  'ɔ': 'yaw',
  'ʌ': 'ye',
  'ə': 'ye',
  'ɐ': 'ye',
  'aɪ': 'yo',
  'aɪɚ': 'yol',
  'aɪɚɹ': 'yol',
  'ɔɪ': 'yoo',
  'ɚ': 'al',
  'ɚɹ': 'al',
  'ɜ': 'al',
  'ɜːɹ': 'al',
  'ʌɹ': 'al',
  'ɔːɹ': 'al',
  'æɹ': 'al',

  // Consonants
  's': 's',
  't': 't',
  'ɾ': 't',
  'n': 'n',
  'əŋ': 'yen',
  'k': 'k',
  'd': 'g',
  'ɡ': 'd',
  'm': 'v',
  'v': 'm',
  'f': 'h',
  'h': 'f',
  'p': 'b',
  'b': 'p',
  'l': 'l',
  'ɹ': 'l',
  'w': 'vw',
  'z': 'zh',
  'ʃ': 'ch',
  'ð': 'sh',
  'θ': 'sh',
  'tʃ': 'j',
  'ʒ': 'j',
  'j': 'z',
  'dʒ': 'z',

  // Special Cases
  'ɪŋ': 'eeng',
  'iːŋɡ': 'eeng',
  'ɪŋɡ': 'eeng',
  'æŋ': 'ang',
  'æŋɡ': 'ang',
  'ɔŋ': 'ong',
  'ʌŋ': 'oong',
  'ɛŋ': 'eng',

  // Ignore suprasegmentals and whitespace
  'ˈ': '',
  'ˌ': '',
  'ː': '',
  ' ': ' ',
  '\n': ' ',
};

const phonemes = Object.keys(transliterations)
  .sort((x, y) => y.length - x.length);

function* tokenizeIPA(ipa: string) {
  for (let ii = 0; ii < ipa.length;) {
    const phoneme = phonemes.find((p) => p === ipa.substr(ii, p.length));
    // istanbul ignore if
    if (!phoneme)
      throw new Error(`No phoneme for '${ipa[ii]}'`);

    yield phoneme;
    ii += phoneme.length;
  }
}

/**
 * Notes from https://www.reddit.com/r/ToothAndTail/comments/76mvba/so_you_want_to_learn_vyeshal/
 *
 * articles like "the" and "a" are omitted
 */
function omitArticles(english: string) {
  return english.replace(/\b(?:an?|the)\s*\b/ig, '');
}

/**
 * Notes from https://www.reddit.com/r/ToothAndTail/comments/76mvba/so_you_want_to_learn_vyeshal/
 *
 * Sometimes when translating you'll end up with consonant sounds that are
 * difficult to pronounce together, in these cases you add "ye" sound in
 * between. Example: freedom --> hyeloogyev (think of it like saying
 * "fuhreedom"
 *
 * Sometimes when this occurs at the end of the word, the "ye" comes after the
 * second consonant. Example: land--> lingye (think of it like saying "landuh"
 */
function addYe(text: string) {
  const regex = buildSurroundRegex([
    ['h', 'l'],
    ['h', 'z'],
    ['l', 'n'],
    ['m', 'l'],
    ['t', 'l'],
    ['[hlmv]g', '\\b'],
    ['[iw]ng', '\\b'],
    ['yong', '\\b'],
    ['[eo]h', '\\b'],
    ['j', '\\b'],
  ]);
  return text.replace(regex, 'ye');
}

/**
 * Notes from https://www.reddit.com/r/ToothAndTail/comments/76mvba/so_you_want_to_learn_vyeshal/
 *
 * the vowels that start with a "y" sound are difficult to pronounce after
 * certain consonants, when this occurs, you precede them with a long "o" sound.
 * Example: right--> loyot
 */
function addO(text: string) {
  const regex = buildSurroundRegex([
    ['(?:\\b|\\bye|k|g|p)l', 'y'],
    ['w', 'y'],
    ['z', 'y'],
  ]);
  return text.replace(regex, 'o');
}

/**
 * Build a regex that matches the position between any of the given `[before,
 * after]` regex pairs.
 */
function buildSurroundRegex(beforeAfters: ReadonlyArray<[string, string]>) {
  const raw = beforeAfters.map(([before, after]) => `(?<=${before})(?=${after})`).join('|');
  return new RegExp(raw, 'g');
}

function matchCase(word: string, reference: string) {
  if (/^[^A-Z]+$/.test(reference))
    return word.toLowerCase();
  if (/^[^a-z][^A-Z]*$/.test(reference))
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  if (/^[^a-z][^A-Z]{2}/.test(reference))
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  if (/^[^a-z]+$/.test(reference))
    return word.toUpperCase();

  // istanbul ignore next
  captureMessage("Can't match case", {
    level: 'warning',
    extra: { word, reference },
   });
  // istanbul ignore next
  return word;
}
