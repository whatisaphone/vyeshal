import { execFile } from 'child_process';
import { captureMessage } from 'raven';
import { promisify } from 'util';

export type Part =
  | { readonly tag: 'word', readonly text: string, readonly ipa: string }
  | { readonly tag: 'extra', readonly text: string };

export async function pronounce(english: string): Promise<ReadonlyArray<Part>> {
  // Ask espeak to pronounce the text. It will return space-separated IPA.
  // Then also split the words ourselves. Both results should have an equal
  // number of words. We can then pair each original word with its
  // pronounciation, while also passing punctuation and etc back unchanged.
  const espeak = await promisify(execFile)(
    'espeak-ng',
    ['-v', 'en-us', '--ipa', preprocess(english)],
  );
  const ipa = espeak.stdout.trim();
  const ipaWords = ipa.split(/\s+/);
  const tokens = [...splitTokens(english)];
  try {
    return [...combine(ipaWords, tokens)];
  } catch (e) {
    if (e instanceof WordCountMismatchError) {
      captureMessage('IPA word count mismatch', {
        level: 'warning',
        extra: { english, ipa, ipaWords, tokens },
      });
      return [{ tag: 'word', text: english, ipa }];
    }

    // istanbul ignore next
    throw e;
  }
}

function preprocess(english: string) {
  // Hack around some words that espeak combines with its neighbors or
  // pronounces weirdly.
  return english.toLowerCase()
    .replace(/\baudio\b/g, 'awdi-o')
    .replace(/\banymore\b/g, 'any-more')
    .replace(/\bdo\b/g, 'doo')
    .replace(/\bfor\b/g, 'four')
    .replace(/\bhas\b/g, 'haz')
    .replace(/\bhave\b/g, 'halve')
    .replace(/\bhello\b/g, 'hell-o')
    .replace(/\bleeroy\b/g, 'leroy')
    .replace(/\bmotherland\b/g, 'mother-land')
    .replace(/\bno\b/g, 'noh')
    .replace(/\bone\b/g, 'won')
    .replace(/\bthere\b/g, 'their')
    .replace(/\bto\b/g, 'too')
    .replace(/\bwe\b/g, 'wee')
    .replace(/\bwonderful\b/g, 'wonderfull');
}

/**
 * Some text paired with a boolean indicating whether it's word-like.
 */
type Token = [boolean, string];

/**
 * Classify text based on whether or not it looks like it's part of a word.
 */
export function* splitTokens(text: string): IterableIterator<Token> {
  const regex = /(?:\w(?:[-'’]\w)?)+/g;
  let lastIndex = 0;
  let match;
  while (match = regex.exec(text)) {
    if (match.index !== lastIndex)
      yield [false, text.slice(lastIndex, match.index)];
    yield [true, match[0]];
    lastIndex = match.index + match[0].length;
  }
  if (text.length !== lastIndex)
    yield [false, text.slice(lastIndex, text.length)];
}

function* combine(
  ipaWords: ReadonlyArray<string>,
  tokens: ReadonlyArray<Token>,
): IterableIterator<Part> {
  let ii = 0;
  let ti = 0;

  while (ti < tokens.length) {
    const ipa = ipaWords[ii];
    const [isWord, text] = tokens[ti];
    if (isWord) {
      if (/^\d+/.test(text)) {
        // Treat digits as 'extra' so they aren't transliterated.
        yield { tag: 'extra', text };
      } else {
        yield { tag: 'word', text, ipa };
      }
      ii += 1;
      ti += 1;
    } else {
      yield { tag: 'extra', text };
      ti += 1;
    }
  }

  if (ii !== ipaWords.length)
    throw new WordCountMismatchError();
}

class WordCountMismatchError extends Error { }
