# vyeshal

[![pipeline status](https://gitlab.com/whatisaphone/vyeshal/badges/master/pipeline.svg)](https://gitlab.com/whatisaphone/vyeshal/commits/master)
[![coverage report](https://gitlab.com/whatisaphone/vyeshal/badges/master/coverage.svg)](https://gitlab.com/whatisaphone/vyeshal/commits/master)

A web service which translates text from English to [Vyeshal],
the fictional language spoken in the [Tooth and Tail] universe.

[Tooth and Tail]: https://en.wikipedia.org/wiki/Tooth_and_Tail
[Vyeshal]: https://www.reddit.com/r/ToothAndTail/comments/76mvba/so_you_want_to_learn_vyeshal/

## Development

### Install prerequisites

* [docker-compose]
* [pre-commit]

[docker-compose]: https://docs.docker.com/compose/
[pre-commit]: https://pre-commit.com

### Start server

```sh
docker-compose -f deploy/env/dev/docker-compose.yml up --build
```

### Run tests

```sh
docker-compose -f deploy/env/dev/docker-compose.yml run server yarn test
```

## Release process

Bump the version number in `package.json`, and then run this script:

```sh
version="v$(jq -r .version <package.json)" && \
git add package.json && \
git commit -m "Release $version" && \
git push && \
git tag -a "$version" -m "Release $version" && \
git push origin "$version"
```

## Reference material

### Vyeshal

* https://drive.google.com/drive/folders/1HSnNtWdOGj7v4r71BKfKN48PZtbqAxiJ
  * Spreadsheet with translations of the game's voice lines
  * Voice audio ripped from the game
* https://www.reddit.com/r/ToothAndTail/comments/76mvba/so_you_want_to_learn_vyeshal/
  * English → Vyeshal translation chart
* http://www.lysator.liu.se/~golen/vyeshal/
  * This is an English ↔ Vyeshal translator that uses precomputed lookup tables.
    It was made by Golen#5309 from the Pocketwatch Discord.
    The lookup tables were auto-generated from the pronounciation rules,
    probably with some post-hoc manual editing.

### IPA (*/ˌɪntɚnˈæʃənəl fənˈɛɾɪk ˈælfəbˌɛt/*)

* https://en.wikipedia.org/wiki/Help:IPA
  * Has a list of IPA symbols with audio files
* https://en.wikipedia.org/wiki/Help:IPA/English
  * Has a list of IPA symbols with example words in English
